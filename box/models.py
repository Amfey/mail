from django.db import models

# Create your models here.

class Email(models.Model):
    adress = models.EmailField(max_length=70)
    subject = models.CharField(max_length=100)
    text = models.TextField()
