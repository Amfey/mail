from django.urls import path
from box.views import BaseViewBox

urlpatterns = [
    path('', BaseViewBox.as_view()),
]