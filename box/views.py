from django.shortcuts import render
from django.views.generic import View, ListView
from box.models import Email

class BaseViewBox(ListView):

    model = Email
    template_name = 'box/base.html'
